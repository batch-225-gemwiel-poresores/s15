// Comments in Javascript
// To write comments, we use two forward slash for single line comments and two fordward slash and two asterisks for multiple-line comments. 
// This is a single Line comment. 

/*
	This is a multi-line comment. 
	To write them, we add two asterisks inside of the forward slashes and write comments in between them. 
*/

// Variables 
/*
	Variable contain values that can be changed over the execution time of the progress. 
	To declare a variable, we use the "let" keyword. 
*/

let productName = 'desktop compute'
console.log (productName)
productName = 'cellphone'
console.log (productName)

// Constants 

/* 
	Use constants for value that will not change. 
*/

const deliveryFee = 30;
console.log (deliveryFee)

// Data Types

// 1. String 
/*
	- Strings are a series of characters that create a word, a phrase, sentence or anything related to "text"
	- Strings in Javascript can be written using a single quote ('') or double quote ("")
	- on other programming languages, only the double quote can be used for creating strings. 
*/

// Example of string

let country = 'Philippines'
let province = "Metro Manila"

let restaurants = 'John\'s Pizza'

// Concatenation - to combine 

console.log (country + ',' + province)

// 2. Numbers

/*
	Include integers/whole numbers, decimal numbers, fraction, exponential notation. 
*/

let headCount = 26;
console.log (headCount);

let grade = 98.27
console.log (grade)

let planetDistance = 2e10
console.log (planetDistance)

let PI=22/7
console.log(PI)

console.log(Math.PI)

console.log ("John's grade last quarter is: " + grade)

 // 3. BOOLEAN
 /*
	- Boolean values are logical values
	- Boolean values can contain either "true" or "false"
 */
let isMarried = false;
let isGoodConduct = true;
console.log ("Kasal na? : " + isMarried);
console.log ("isGoodConduct: " + isGoodConduct)
console.log (isGoodConduct)
console.log (isMarried)

// 3. Objects

	// Arrays 
	/*
		- They are a special kind of data that stores multiple values. 
		- they are a special type of an object.
		- They can store different data ypes but is normally used to store similar data types.
	*/
	// Syntax: 
		// let/const arrayName = [ElementA, ElementB, Element...}]

	let grades = [ 98.7, 92.1, 90.2, 94.6 ];
	console.log (grades)
	console.log (grades[0])

		let userDetails = ["John", "Smith", 32, true]
	console.log(userDetails)

	// Object Literals 
	/*
		- Objects are another special kind of data type that mimics real world object/items. 
		- They are used to create complex data that contains pieces of information that are relevant to each other. 
		- Every individual piece of information if called a property of the object. 

		Syntax: 

			let/const objectName = (
				propertyA: value
				propertyB: value
			)
	*/

	let personDetails= {
		fullName: 'Juan Dela Cruz', 
		age: 35,
		isMarried: false, 
		contact: ['09568471848', '09756460359'],
		address: {
			housenumber: '135', 
			city:'malabon'
			}
	}
	console.log(personDetails)

















